import { Config } from '@stencil/core';
import path from 'path';

export const config: Config = {
  namespace: 'NUIverse',
  outputTargets:[
    { type: 'dist' },
    { type: 'docs' },
    {
      type: 'www',
      serviceWorker: null // disable service workers
    }
  ],
  bundles: [
    { components: ['nuiverse-button'] },
  ],
  devServer: {
    openBrowser: false
  },
  srcDir: 'src/src_components',
};
