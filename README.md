> ## 🛠 Status: In Development
> NUIverse is currently in development. We have come to version 1.0.0, however we have a lot to do, we are currently working on adding main features for the complete use of NUIverse in other projects. Documentation is missing for now.

<div style="text-align:center;">
    <p align="center"><img src="https://raw.githubusercontent.com/Nuinalp/nuiverse/master/nuiverse-logo.png" alt="NUIverse logo" width="300"/></p>
    <p align="center">
      <a href="https://gitlab.com/nuinalp/nuiverse/framework/blob/master/LICENSE.md"><img src="https://img.shields.io/badge/License-BSD-blue.svg" alt="License"/></a>
      <a><img src="https://img.shields.io/badge/Version-1.0.0-green.svg" alt="Version"/></a>
      <a href="https://gitlab.com/nuinalp/nuiverse/framework/pipelines" target="_blank"><img src="https://img.shields.io/badge/Build-passing-green.svg" alt="Version"/></a>
      <a href="https://www.npmjs.com/package/@nuinalp/nuiverse" target="_blank"><img src="https://img.shields.io/npm/v/@nuinalp/nuiverse.svg" alt="Published on npm"><a>
    </p>
</div>

## Introduction

NUIverse is an open source project, licensed under the BSD Type 3 license and based on our design language (Nuinalp Design).

We are passionate about the Web, and we try to develop everything we can using the best of this platform, however with the growing number of frameworks for this purpose, it was difficult to choose one that was compatible with what we thought about design, so we decided to develop our framework to facilitate the development of our products and services.

It is in continuous development and relying on the latest design trends, thus creating a modern, responsive and easy-to-use framework, we also draw on the best frameworks to develop NUIverse and to make it universal offer web component support , so you can use our components in React, Angular, Vue.js, Ember or any other javascript framework :)

Our goal is to create a lightweight, robust and complete framework for creating web interfaces rich in design, animation and style.

We are currently developing NUIverse for the creation of Plánium OS, our operating system and our websites.

## Getting Started

### Quick-start CDN
Start a project quickly using a CDN and let NUIverse do the magic :)

```html
<!DOCTYPE html>
<html>
<head>
  <link href="https://cdn.jsdelivr.net/npm/@nuinalp/nuiverse/dist/css/nuiverse.min.css" rel="stylesheet">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
</head>
<body>
  <div id="app">
    <h1>Hello World :)</h1>
  </div>
</body>
</html>
```

### Install in project via NPM

```bash
$ npm install @nuinalp/nuiverse
```

## Changelog

Detailed changes for each release are documented in the [release notes](https://gitlab.com/nuinalp/nuiverse/framework/tags).

## Creators

**Patrick A Lima**

- [Twitter](https://twitter.com/patrickalima98)
- [GitLab](https://gitlab.com/patrickalima98)

**Marcelo Martins**

- [Twitter](https://twitter.com/thanakin)
- [GitLab](https://gitlab.com/thanakin)

## Support
Support NUIverse at [Patreon](https://www.patreon.com/nuinalp) to help sustain project development. If you or your company uses or depends on our project, why not help us with a donation? We work in open source for free to help everyone achieve more. Access Patreon, make your contribution and support this project :-).

## License

[BSD Clause 3](https://opensource.org/licenses/BSD-3-Clause)

<p>Copyright (c) 2018 The Nuinalp Authors. All rights reserved.<br>
Use of this source code is governed by a BSD-style license that can be found in the LICENSE file.</p>
