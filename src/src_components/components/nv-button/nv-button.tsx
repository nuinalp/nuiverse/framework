import { Component, Prop, Watch, Element, State  } from '@stencil/core';

@Component({
  tag: 'nuiverse-button',
  shadow: false
})
export class NuiverseButton {
  @Prop({
    reflectToAttr: false
  })

  @Prop() text: string;
  @Prop() background: string;
  @Prop() textColor: string;
  @Prop() class: string;
  @Prop() radius: string;
  @Prop() to: string;
  @State() body: HTMLElement = document.querySelector('body');
  @Element () Element: HTMLElement;
  
  constructor () {
    let win = (window as any);
    win.NUIverse = win.NUIverse || {};
  }

  componentDidLoad() {
    this.addFxClick(this.Element);
  }

  addFxClick(el:HTMLElement) {
    let checkAnim:string = this.checkAnimation(),
        fxClick:HTMLElement = el.querySelector('.fx-click');
    
    fxClick.addEventListener(checkAnim, () => {
      window.requestAnimationFrame(()=> {
        fxClick.classList.remove('fx-running');
      });
    });
  }

  onMouseMove(e:any) {
    // if (!this.applyHover || e.target.className.indexOf('watch-wrapper') === -1) {
    //   return;
    // }
    e.preventDefault();
    e.stopPropagation();

    let fxClick:any = this.Element.getElementsByClassName('fx-click')[0],
        fxHover:any = this.Element.getElementsByClassName('fx-hover')[0],
        x = (e.pageX - fxClick.offsetWidth / 2) - e.currentTarget.offsetLeft,
        y = (e.pageY - fxClick.offsetHeight / 2) - e.currentTarget.offsetTop;

    window.requestAnimationFrame(()=> {
      fxClick.style.setProperty('left', `${ x }px`);
      fxClick.style.setProperty('top', `${ y }px`);

      fxHover.style.setProperty('left', `${ x }px`);
      fxHover.style.setProperty('top', `${ y }px`);
    });
  }

  clickFx() {
    let fxClick:HTMLElement = this.Element.querySelector('.fx-click');
    window.requestAnimationFrame(()=> {
        fxClick.classList.add('fx-running');
    });
  }

  // Check animation support for transtions and animations
  checkAnimation():string {
    let checkAnim = (window as any).NUIverse.checkAnimation;
    if(!checkAnim) {
      let t;
      let el = document.createElement('fakeelement');
      let transitions = {
        'animation':'animationend',
        'OAnimation':'oAnimationEnd',
        'MozOAnimation':'animationend',
        'WebkitOAnimation':'webkitAnimationEnd'
      }

      for(t in transitions) {
        if(el.style[t] !== undefined) {
          // (window as any).NUIverse.checkAnimation = transitions[t];
          return transitions[t];
        }
      }
    }
  }

  componentWillUpdate() {
    this.insertClass();
    this.changeColor();
    this.changeBackground();
    this.changeRadius();
  }

  textSpan(): string {
    return this.text ? ` ${this.text}` : '';
  }

  // 
  // APIs
  // 

  @Watch('class')
  insertClass() {
    let newClass:string = this.class.replace(/hydrated/gi, '').trim();
    this.Element.querySelector('.button').className = 'button ' + newClass;
  }

  @Watch('textColor')
  changeColor() {
    let span = this.Element.querySelector('.button span') as HTMLElement;
    span.style.color = this.textColor;
  }

  @Watch('background')
  changeBackground() {
    let button = this.Element.querySelector('.button') as HTMLElement;
    button.style.backgroundColor = this.background;
  }
  
  @Watch('radius')
  changeRadius() {
    let button = this.Element.querySelector('.button') as HTMLElement;
    button.style.borderRadius = this.radius;
  }
  
  @Watch('to')
  navegateTo() {
    let anim:any = (window as any).NUIverse.checkAnimation;

    this.body.addEventListener(anim, () => {
      window.location.href = this.to;
    }, false);

    window.requestAnimationFrame(()=> {
      this.body.classList.add('page-fade-out');
    });
  }
  
  render() {
    return (
      <button class="button" onMouseMove={(event) => {this.onMouseMove(event)}} onClick={() => this.clickFx()}>
        <span class="nfw-button-text">{this.textSpan()}</span>
        <div class="fx-click"></div>
        <div class="fx-hover"></div>
      </button>
    );
  }
}
