# nuiverse-button



<!-- Auto Generated Below -->


## Properties

| Property     | Attribute    | Description | Type     | Default     |
| ------------ | ------------ | ----------- | -------- | ----------- |
| `background` | `background` |             | `string` | `undefined` |
| `class`      | `class`      |             | `string` | `undefined` |
| `radius`     | `radius`     |             | `string` | `undefined` |
| `text`       | `text`       |             | `string` | `undefined` |
| `textColor`  | `text-color` |             | `string` | `undefined` |
| `to`         | `to`         |             | `string` | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
