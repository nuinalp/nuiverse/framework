const path = require('path')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const StyleLintPlugin = require('stylelint-webpack-plugin');
const devMode = process.env.NODE_ENV !== 'production'
const config = require('./config');
module.exports = {
  mode: 'production',
  devtool: 'inline-source-map',
  entry: path.resolve(__dirname, '../src/index.ts'),
  output: {
    path: path.resolve(__dirname, '../dist'),
    filename: 'nuiverse.min.js',
    publicPath: process.env.NODE_ENV === 'production'
      ? config.build.assetsPublicPath
      : config.dev.assetsPublicPath
  },
  module: {
    rules: [{
      test: /\.scss$/,
      loader: [
        {loader: devMode ? 'style-loader' : MiniCssExtractPlugin.loader},
        {loader: "css-loader", options: {sourceMap: true}},
        {loader: "postcss-loader",  options: {sourceMap: true} },
        {loader: "sass-loader", options: {sourceMap: true} }
      ]
    },
    {
      test: /\.tsx?$/,
      use: 'ts-loader',
      exclude: /node_modules/
    },
    { 
      test: /\.(png|woff|woff2|eot|ttf|svg)$/,
      loader: 'url-loader?limit=100000'
    }
  ]
  },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ]
  },
  plugins: [
    new StyleLintPlugin({
      configFile: '.stylelintrc'
    }),
  ]
};